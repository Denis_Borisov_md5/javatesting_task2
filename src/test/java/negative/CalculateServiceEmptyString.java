package negative;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import service.GetCalculatableString;
import service.ImplGetCalculatableString;

public class CalculateServiceEmptyString {
    private ImplGetCalculatableString getCalculatableString = new GetCalculatableString();

    private String testString = " ";

    @Test
    public void CalculateService_EMPTY_STRING(){
        System.out.println("Тестирование на пустую строку");
        String expected = getCalculatableString.getTokenString(testString);
        assertThat(expected, is(isEmptyString()));
    }
}
