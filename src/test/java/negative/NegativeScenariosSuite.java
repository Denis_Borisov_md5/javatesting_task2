package negative;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CalculateServiceDivisionByZero.class,
        CalculateServiceEmptyString.class})
public class NegativeScenariosSuite {
}
