package negative;

import exceptions.DivideByZeroException;
import org.junit.Test;
import service.CalculateService;
import service.ImplCalculateService;

public class CalculateServiceDivisionByZero {

    private ImplCalculateService calculateService = new CalculateService();

    private double first = 0;
    private double second = 9;

    @Test(expected = DivideByZeroException.class)
    public void calculateService_DIVISION_BY_ZERO()throws DivideByZeroException{
        System.out.println("Тестирование деления на ноль");
        calculateService.division(first, second);
    }
}
