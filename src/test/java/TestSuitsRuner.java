import negative.NegativeScenariosSuite;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import positive.PositiveScenariosSuite;

public class TestSuitsRuner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(NegativeScenariosSuite.class, PositiveScenariosSuite.class);

        for(Failure failure : result.getFailures()){
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }
}
