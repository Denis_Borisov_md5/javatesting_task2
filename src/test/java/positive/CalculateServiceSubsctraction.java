package positive;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import service.CalculateService;
import service.GetCalculatableString;
import service.ImplCalculateService;
import service.ImplGetCalculatableString;

public class CalculateServiceSubsctraction {

    private ImplCalculateService calculateService = new CalculateService();
    private ImplGetCalculatableString getCalculatableString = new GetCalculatableString();

    private String testString = "5 - 2";
    private Double testSubstraction = (double)(5 - 2);

    @Test
    public void calculateService_SUBSTRACTION(){
        System.out.println("Тестирование вычитания");
        Double expected = testSubstraction;
        Double current = calculateService.calculateResult(getCalculatableString.getTokenString(testString));
        assertThat(expected, equalTo(current));
    }
}
