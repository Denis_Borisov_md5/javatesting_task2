package positive;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import service.CalculateService;
import service.GetCalculatableString;
import service.ImplCalculateService;
import service.ImplGetCalculatableString;

public class CalculateServiceMultiplication {

    private ImplCalculateService calculateService = new CalculateService();
    private ImplGetCalculatableString getCalculatableString = new GetCalculatableString();

    private String testString = "3 * 3";
    private Double testMultiplication = (double)(3 * 3);

    @Test
    public void calculateService_MULTIPLICATION(){
        System.out.println("Тестирование умножения");
        Double expected = testMultiplication;
        Double current = calculateService.calculateResult(getCalculatableString.getTokenString(testString));
        assertThat(expected, equalTo(current));
    }
}
