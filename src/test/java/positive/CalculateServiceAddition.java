package positive;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import service.CalculateService;
import service.GetCalculatableString;
import service.ImplCalculateService;
import service.ImplGetCalculatableString;

public class CalculateServiceAddition {

    private ImplCalculateService calculateService = new CalculateService();
    private ImplGetCalculatableString getCalculatableString = new GetCalculatableString();

    private String testString = "2 + 2";
    private Double testSum = (double)(2 + 2);

    @Test
    public void calculateService_ADDITION(){
        System.out.println("Тестирование сложения");
        Double expected = testSum;
        Double current = calculateService.calculateResult(getCalculatableString.getTokenString(testString));
        assertThat(expected, equalTo(current));
    }
}
