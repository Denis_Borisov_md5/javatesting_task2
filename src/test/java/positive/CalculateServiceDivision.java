package positive;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import service.CalculateService;
import service.GetCalculatableString;
import service.ImplCalculateService;
import service.ImplGetCalculatableString;

public class CalculateServiceDivision {

    private ImplCalculateService calculateService = new CalculateService();
    private ImplGetCalculatableString getCalculatableString = new GetCalculatableString();

    private String testString = "9 / 3";
    private Double testDivision = (double)(9 / 3);

    @Test
    public void calculateService_DIVISION(){
        System.out.println("Тестирование деления");
        Double expected = testDivision;
        Double current = calculateService.calculateResult(getCalculatableString.getTokenString(testString));
        assertThat(expected, equalTo(current));
    }
}
