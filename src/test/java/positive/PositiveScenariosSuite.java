package positive;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CalculateServiceDivision.class,
        CalculateServiceMultiplication.class,
        CalculateServiceSubsctraction.class,
        CalculateServiceAddition.class
})
public class PositiveScenariosSuite {
}
