package service;

import exceptions.DivideByZeroException;

public interface ImplCalculateService {
    double calculateResult(String tokenString);
    double division(double first, double second) throws DivideByZeroException;
}
