package service;

import exceptions.DivideByZeroException;

import java.util.Stack;

public class CalculateService implements ImplCalculateService{
    private  ImplCheckService checkService = new CheckService();

    private Stack<String> stackOfStringTokens = new Stack<>();

    @Override
    public double division(double first, double second) throws DivideByZeroException {
        if(first == 0){
            throw new DivideByZeroException("На ноль делить нельзя!");
        }else {
            return second/first;
        }
    }

    @Override
    public double calculateResult(String tokenString){

        for (String currentSymbol : tokenString.split(" ")) {
            if (checkService.isNumber(currentSymbol)) {
                stackOfStringTokens.push(currentSymbol);
                continue;
            }

            if (checkService.isOperator(currentSymbol)) {
                double result = 0;
                double first = Double.parseDouble(stackOfStringTokens.pop());
                double second = Double.parseDouble(stackOfStringTokens.pop());

                switch (currentSymbol) {
                    case "/":
                        try{
                            result = division(first, second);
                            break;
                        }catch (DivideByZeroException ex){
                            System.out.println(ex.getMessage());
                        }
                    case "*":
                        result = second * first;
                        break;
                    case "+":
                        result = second + first;
                        break;
                    case "-":
                        result = second - first;
                        break;
                }
                stackOfStringTokens.push(String.valueOf(result));
            }
        }
        return Double.parseDouble(stackOfStringTokens.pop());
    }
}
