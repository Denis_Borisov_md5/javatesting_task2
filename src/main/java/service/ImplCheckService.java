package service;

public interface ImplCheckService {
    boolean isNumber(String currentSymbol);
    boolean isOperator(String currentSymbol);
    int priorytyCheck(String currentSymbol);
}
