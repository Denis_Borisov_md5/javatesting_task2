package service;

public class CheckService implements ImplCheckService{
    public boolean isNumber(String currentSymbol){
        try{
            Double.parseDouble(currentSymbol);
            return true;
        } catch (Exception e){
            return false;
        }
    }
    public boolean isOperator(String currentSymbol){
        return currentSymbol.equals("+") || currentSymbol.equals("-") ||
                currentSymbol.equals("*") || currentSymbol.equals("/");
    }

    public int priorytyCheck(String currentSymbol){
        switch (currentSymbol){
            case "*":
            case "/":
                return 2;
            case "+":
            case "-":
                return 1;
            default:
                return 0;
        }
    }
}
