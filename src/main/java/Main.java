import service.CalculateService;
import service.GetCalculatableString;
import service.ImplCalculateService;
import service.ImplGetCalculatableString;

public class Main {
    public static void main(String[] args) {
        ImplCalculateService calculateService = new CalculateService();
        ImplGetCalculatableString getCalculatableString = new GetCalculatableString();


        String inputString = "( 1 + 2 ) * 4 - 58 + 99 / 3 ";
        System.out.println("Input string: " + inputString);

        System.out.println("Result: " + calculateService.calculateResult(getCalculatableString.
                    getTokenString(inputString)));
    }
}
